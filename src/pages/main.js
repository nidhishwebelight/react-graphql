import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Users from "../components/users";
import AddUser from "../components/adduser";
import UpdateUser from "../components/updateuser";
import DeleteUser from "../components/deleteuser";

const MainComponent = () => {
    return (
        <Router>
            <div>
                <h1>React-GraphQL</h1>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/adduser">Add user</Link>
                        </li>
                        <li>
                            <Link to="/updateuser">Update user</Link>
                        </li>
                        <li>
                            <Link to="/deleteuser">Delete user</Link>
                        </li>
                    </ul>
                </nav>
            </div>
            <Switch>
                <Route exact path="/" ><Users /></Route>
                <Route path="/adduser" >< AddUser /></Route>           
                <Route path="/updateuser" ><UpdateUser /></Route>
                <Route path="/deleteuser" ><DeleteUser /></Route>
            </Switch>
        </Router>
    )
}

export default MainComponent;