import { gql } from '@apollo/client';

export const ADD_USER = gql`
  mutation CreateUser($name: String!, $email: String!, $age: Int!, $imageUrl: String!) {
    createUser(data: { name: $name, email: $email, age: $age, imageUrl: $imageUrl }) {
      __typename
      id
      name
      email
      age
      imageUrl
    }
  }
`;

export const DELETE_USER = gql`
    mutation DeleteUser($id: ID!) {
        deleteUser(id: $id){
            id
            name
        }
    }
`;

export const UPDATE_USER = gql`
    mutation UpdateUser($id: ID!, $name: String!, $email: String!, $age: Int!) {
        updateUser(id: $id, data: { name: $name, email: $email, age: $age }) {
            id
            name
            email
            age
        }
    }
`;

export const GET_USERS = gql`
{    
    users {
        id
        name
        email
        age
        posts {
          id
          title
        }
    }
}
`;

export const GET_SIGNED_URL = gql`
    mutation GetSignedUrl($fileName: String!, $fileType: String!) {
        getSignedUrl(data: { fileName: $fileName, fileType: $fileType }) {
            url
            signedRequest
        }
    }
`;