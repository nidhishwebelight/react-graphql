import React from 'react';

import ReactDOM from 'react-dom';

import './index.css';

import MainComponent from './pages/main';

import { ApolloProvider } from '@apollo/client';

import client from "./apollo-client/index";

ReactDOM.render(<ApolloProvider client={client}><MainComponent /></ApolloProvider>, document.getElementById('root'));