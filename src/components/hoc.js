import React from "react";

const Hoc = (WrappedComponent) => (props) => {
    localStorage.setItem("user", JSON.stringify({ name: "Rahul", age: 22 }));
    const user = JSON.parse(localStorage.getItem("user"));
    return (
      <div>
        <WrappedComponent userDetails={user} {...props} />
      </div>
    )
}

export default Hoc;