import React from "react";

import { useMutation } from '@apollo/client';

import { DELETE_USER } from "../graphql-query/index";

import { useFormik } from 'formik';

import * as Yup from "yup";

const validationSchema = Yup.object().shape({
    id: Yup.string()
        .required('Required')
});

const DeleteUser = () => {
    const [deleteUser, { loading, error, data }] = useMutation(DELETE_USER, {
        update(cache, { data }) {
            const { deleteUser } = data;
            cache.modify({
                id: cache.identify(deleteUser),
                fields: {
                  users(existingUsersRefs, { readField  }) {
                    return existingUsersRefs.filter(
                        (userRef) => deleteUser.id !== readField('id', userRef) 
                    );
                  },
                },
              });
        }
    });
    const { handleSubmit, handleChange, values, errors, touched } = useFormik({
        initialValues: {
            id: ""
        },
        validationSchema,
        onSubmit: (values, { resetForm }) => {
            const { id } = values;
            deleteUser({ variables: { id } });
            resetForm();
        }
    })
    return (
        <div>
            <h3>Delete a user</h3>
            <form onSubmit={handleSubmit}>
                <label htmlFor="id">Id: </label>
                <input type="text" id="id" placeholder="Enter id" onChange={handleChange} value={values.id} /><br />
                {errors.id && touched.id ? <div>{errors.id}</div> : <br />}
                <button type="submit">Delete user</button>
            </form>
            {loading && <p>Loading...</p>}
            {error && <p>Error</p>}
            {data && <p>User deleted</p>}
        </div>
    )
}

export default DeleteUser;