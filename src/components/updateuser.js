import React from "react";

import { useMutation } from '@apollo/client';

import { UPDATE_USER } from "../graphql-query/index";

import { useFormik } from 'formik';

import * as Yup from "yup";

const validationSchema = Yup.object().shape({
    id: Yup.string()
        .required('Required'),
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    age: Yup.string()
        .required('Required')
});

const UpdateUser = () => {
    const [updateUser, { loading, error, data }] = useMutation(UPDATE_USER);
    const { handleSubmit, handleChange, values, errors, touched } = useFormik({
        initialValues: {
            id: "",
            name: "",
            email: "",
            age: ""
        },
        validationSchema,
        onSubmit: (values, { resetForm }) => {
            const { id, name, email, age } = values;
            updateUser({ variables: { id, name, email, age: parseInt(age) } });
            resetForm();
        }
    })
    return (
        <div>
            <h3>Update a user</h3>
            <form onSubmit={handleSubmit}>
                <label htmlFor="id">Id: </label>
                <input type="text" id="id" placeholder="Enter id" onChange={handleChange} value={values.id} /><br />
                {errors.id && touched.id ? <div>{errors.id}</div> : <br />}
                <label htmlFor="name">Name: </label>
                <input type="text" id="name" placeholder="Enter name" onChange={handleChange} value={values.name} /><br />
                {errors.name && touched.name ? <div>{errors.name}</div> : <br />}
                <label htmlFor="email">Email: </label>
                <input type="email" id="email" placeholder="Enter email" onChange={handleChange} value={values.email} /><br />
                {errors.email && touched.email ? <div>{errors.email}</div> : <br />}
                <label htmlFor="age">Age: </label>
                <input type="number" id="age" placeholder="Enter age" onChange={handleChange} value={values.age} /><br />
                {errors.age && touched.age ? <div>{errors.age}</div> : <br />}
                <button type="submit">Update user</button>
            </form>
            {loading && <p>Loading...</p>}
            {error && <p>Error</p>}
            {data && <p>User updated</p>}
        </div>
    )
}

export default UpdateUser;