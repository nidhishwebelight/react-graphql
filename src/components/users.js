import React from "react";

import { useQuery } from '@apollo/client';

import { GET_USERS } from "../graphql-query/index";

const Users = () => {
    const { loading, error, data, refetch } = useQuery(GET_USERS, { notifyOnNetworkStatusChange: true });
    if (loading) {
        return <p>Loading...</p>;
    }
    if (error) {
        return <p>Error</p>;
    }
    return (
        <div>
            <h2>List of users</h2>
            {
                data.users.map((user) => {
                    return (
                        <div key={user.id}>
                            <h3 key={user.id}>Name: {user.name}</h3>
                            <h4>Posts: {user.posts.length}</h4>
                            {user.posts.length > 0 && (<ol>
                                {
                                    user.posts.map((post) => {
                                        return <li key={post.id}>Title: {post.title}</li>
                                    })
                                }
                            </ol>)
                            }
                        </div>
                    )
                })
            }
            <button onClick={() => { return refetch() }}>Refetch</button>
        </div>
    )
}

export default Users;