import React, { useState } from "react";

import { useMutation, gql } from '@apollo/client';

import { ADD_USER } from "../graphql-query/index";
import { GET_SIGNED_URL } from "../graphql-query/index";

import { useFormik } from 'formik';

import axios from 'axios';

import * as Yup from "yup";

const validationSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    age: Yup.string()
        .required('Required')
});

const AddUser = () => {
    const [selectedimageFile, setSelectedImageFile] = useState(null);
    const [getSignedUrl, { loading: getSignedUrlLoading, error: getSignedUrlError, data: getSignedUrlData }] = useMutation(GET_SIGNED_URL);
    const [addUser, { loading, error, data }] = useMutation(ADD_USER, {
        update(cache, { data }) {
            const { createUser } = data;
            cache.modify({
                fields: {
                    users(existingUserRefs = [], { readField }) {
                        const newUserRef = cache.writeFragment({
                            data: createUser,
                            fragment: gql`
                        fragment NewUser on Users {
                          id
                          name
                          email
                          age
                          imageUrl
                        }
                      `
                        });
                        if (existingUserRefs.some(
                            ref => readField('id', ref) === createUser.id
                        )) {
                            return existingUserRefs;
                        }
                        return [...existingUserRefs, newUserRef];
                    }
                }
            });
        }
    });
    const { handleSubmit, handleChange, values, errors, touched } = useFormik({
        initialValues: {
            name: "",
            email: "",
            age: ""
        },
        validationSchema,
        onSubmit: (values, { resetForm }) => {
            if (selectedimageFile) {
                const fileName = selectedimageFile.name;
                const fileType = selectedimageFile.type;
                getSignedUrl({ variables: { fileName, fileType } });
            }
        }
    })
    function imageChangedHandler(e) {
        setSelectedImageFile(e.target.files[0]);
    }
    if (getSignedUrlData && !data && !loading && !error) {
        const fileType = selectedimageFile.type;
        const { signedRequest, url: imageUrl } = getSignedUrlData.getSignedUrl;
        const { name, email, age } = values;
        const options = {
            headers: {
                'Content-Type': fileType,
            }
        };
        const sendPutRequest = async () => {
            try {
                await axios.put(signedRequest, selectedimageFile, options);
                addUser({ variables: { name, email, age: parseInt(age), imageUrl } });
            } catch (e) {
                console.log("Error: ",e);
                alert("Something went wrong");
            }
        };
        sendPutRequest();
    }
    return (
        <div>
            <h3>Add a new user</h3>
            <form onSubmit={handleSubmit}>
                <label htmlFor="name">Name: </label>
                <input type="text" id="name" placeholder="Enter name" onChange={handleChange} value={values.name} /><br />
                {errors.name && touched.name ? <div>{errors.name}</div> : <br />}
                <label htmlFor="email">Email: </label>
                <input type="email" id="email" placeholder="Enter email" onChange={handleChange} value={values.email} /><br />
                {errors.email && touched.email ? <div>{errors.email}</div> : <br />}
                <label htmlFor="age">Age: </label>
                <input type="number" id="age" placeholder="Enter age" onChange={handleChange} value={values.age} /><br />
                {errors.age && touched.age ? <div>{errors.age}</div> : <br />}
                <label>Profile photo: </label>
                <input type="file" onChange={imageChangedHandler} required="required" /><br />
                <button type="submit">Add user</button>
            </form>
            {(loading || getSignedUrlLoading) && <p>Loading...</p>}
            {(error || getSignedUrlError) && <p>Error</p>}
            {data && <p>User added</p>}
        </div>
    )
}

export default AddUser;